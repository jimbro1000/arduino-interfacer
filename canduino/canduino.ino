#include <Canbus.h>
#include <defaults.h>
#include <global.h>
#include <mcp2515.h>
#include <mcp2515_defs.h>

String response;
bool stringComplete = false;
String inputString = "";
unsigned long lastTimer = 0;
unsigned long lastConnectTimer = 0;
bool canInit = false;

void setup() {
  Serial.begin(38400);
  inputString.reserve(10);
  initCan();
}

void initCan()
{
  if (Canbus.init(CANSPEED_500))
  {
    canInit = true;
  }
}

void loop() {
  tCAN message;
  unsigned long currentTime = millis();
  unsigned long delta = currentTime - lastTimer;
  unsigned long deltaConnect = currentTime - lastConnectTimer;
  if (canInit)
  {
    if (mcp2515_check_message())
    {
      if (mcp2515_get_message(&message))
      {
        // do something
      }
    }
  }
  else
  {
    if (deltaConnect > 2000) {
      initCan();
    }
  }
  if (delta > 200) {
    lastTimer = currentTime;
    build_response(0, 0, 0, 0, 0, 0, currentTime);
  }
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      stringComplete = true;
    } else {
      inputString += inChar;
    }
  }
  if (stringComplete){
    stringComplete = false;
    if (inputString == "r") {
      Serial.println(response);
    }
    inputString = "";
  }
}

void build_response(float revs, float road_speed, float water_temp, float oil_pressure, float fuel_level, int signals, unsigned long timer) {
  String result = "{";
  result.concat("\"connected\":");
  if (canInit) {
    result.concat("1");
  } else {
    result.concat("0");
  }
  result.concat(",\"revs\":");
  result+=String(revs,0);
  result.concat(",\"speed\":");
  result+=String(road_speed,0);
  result.concat(",\"water\":");
  result+=String(water_temp,1);
  result.concat(",\"oil\":");
  result+=String(oil_pressure,1);
  result.concat(",\"fuel\":");
  result+=String(fuel_level,3);
  result.concat(",\"signals\":");
  result+=String(signals);
  result.concat(",\"timer\":");
  result+=String(timer);
  result.concat("}");
  response = result;
}
