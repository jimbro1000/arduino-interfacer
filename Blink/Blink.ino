/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

char data;
String response;
int state;
bool stringComplete = false;
String inputString = "";
unsigned long lastTimer = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize serial:
  Serial.begin(38400);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  state = HIGH;
  digitalWrite(LED_BUILTIN, state);
  inputString.reserve(10);
}

// the loop function runs over and over again forever
void loop() {
  unsigned long currentTime = millis();
  unsigned long delta = currentTime - lastTimer;
  if (delta > 200) {
    lastTimer = currentTime;
    build_response(0, 0, 0, 0, 0, 0);
    Serial.println("response updated");
  }
//  data = ' ';
//  while (Serial.available())
//  {
//    data = Serial.read();
//  }
//
//  if (data == '1')
//  {
//    state = HIGH;
//    digitalWrite(LED_BUILTIN, state);
//    Serial.println("LED turned on");
//  }
//  else if (data == '0')
//  {
//    state = LOW;
//    digitalWrite(LED_BUILTIN, state);
//    Serial.println("LED turned off");
//  }
//  else if (data == '2')
//  {
//    if (state == LOW)
//    {
//      state = HIGH;
//    }
//    else
//    {
//      state = LOW;
//    }
//    digitalWrite(LED_BUILTIN, state);
//    Serial.println("LED toggled");
//  }
//  else if (data =='r')
//  {
//    build_response(0, 0, 0, 0, 0, 0);
//    Serial.println(response);
//  }
}

void serialEvent() {
  Serial.println("serial event");
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } else {
      inputString += inChar;
    }
  }
  Serial.println("Input = " + inputString);
  if (stringComplete) {
    Serial.println("stringComplete = True");
  }
  else 
  {
    Serial.println("stringComplete = False");
  }
  if (stringComplete){
    stringComplete = false;
    if (inputString == "r") {
      Serial.println("Response =" + response);
    } else if (inputString == "2") {
      if (state == LOW)
      {
        state = HIGH;
      }
      else
      {
        state = LOW;
      }
      digitalWrite(LED_BUILTIN, state);
      Serial.println("LED toggled");
    }
    inputString = "";
  }
}

void build_response(float revs, float road_speed, float water_temp, float oil_pressure, float fuel_level, int signals) {
  String result = "{";
  result.concat("revs:");
  result+=String(revs,0);
  result.concat(",speed:");
  result+=String(road_speed,0);
  result.concat(",water:");
  result+=String(water_temp,1);
  result.concat(",oil:");
  result+=String(oil_pressure,1);
  result.concat(",fuel:");
  result+=String(fuel_level,3);
  result.concat(",signals:");
  result+=String(signals);
  result.concat(")");
  response = result;
}
