*README*

The arduino-interfacer sketch collects CAN-Bus messages, filters 
them and repackages them into a data feed for the main piDash package

**Requirements**

***Ardunio Uno***
The sketch is intended for use with an Arduino Uno (or compatible) -
I use an Uno R3 purely for the micro-usb connection

***CAN-Bus Shield***
The board used needs to accept the SparkFun CAN-Bus Shield

**Dependencies**
This package is dependent on the SparkFun CAN-Bus library

https://github.com/sparkfun/SparkFun_CAN-Bus_Arduino_Library
